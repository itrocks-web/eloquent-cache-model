<?php

namespace WerdenIt\EloquentModelCache\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class ModelCache extends Model
{
    use HasFactory;

    protected static $index_cache_prefix = 'cache_model_';
    protected static $ttl_cache = 3600;

    /**
     * Get all of the models from the database.
     *
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function all($columns = ['*'])
    {
        if($columns == ['*']){
            if(Cache::has(self::getCacheIndex())){
                return Cache::get(self::getCacheIndex());
            }

            $items = parent::all($columns);

            Cache::put(self::getCacheIndex(), $items, self::$ttl_cache);

            return $items;
        }

        return parent::all($columns);
    }

    protected static function getCacheIndex(){
        return self::$index_cache_prefix .  (new static)->table;
    }
}
